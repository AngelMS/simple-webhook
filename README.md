# Simple Webhook

Simple Webhook - nodejs + express + body-parser app to recieve push notifications


### Install dependencies
`npm i`

### Run the app
`npm start`

### Check if its working
 - GET `go to http://localhost:3000` and get a Hello World Message
 - POST run in terminal: `curl -H "Content-type: application/json" -d '{"param1": "aaa","ppp": "bbb"}' 'localhost:3000'`
