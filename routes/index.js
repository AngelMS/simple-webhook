"use strict"

const express = require("express")
const api = express.Router()



api.get("/", (req, res) => {
	res.status(200).send({ message: `Hello World - Simple Webhook server up and running - send my a post request` })
})


api.post("/", async (req, res) => {
	let data = req.body
	let param1 = req.body.param1 // -<- POST params are automatically parsed like this
	let param2 = req.body.param2

	console.log('> POST data: ', data)
	console.log('> param1: ', param1)
	console.log('> param2: ', param2)


	res.status(200).send({ message: "POST data revieved, check your backend logs"})
})



module.exports = api
